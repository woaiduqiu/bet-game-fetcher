'use strict';
let mongoose = require('mongoose');
mongoose.Promise = global.Promise;
let dbOptions = {
    // server: { poolSize: 5 },    
    // replset: { rs_name: 'myReplicaSetName' },//复制
    // user: 'myUserName',
    // pass: 'myPassword'
};

mongoose.connect('mongodb://localhost/BetDB',dbOptions);

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.info('connected');
});
db.on('disconnected', function () {
  console.log('Mongoose default connection disconnected');
});
process.on('SIGINT', function() {
  db.close(function () {
    console.log('Mongoose default connection disconnected through app termination');
    process.exit(0);
  });
});

module.exports = db;