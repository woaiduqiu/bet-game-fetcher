'use strict';
let request = require('superagent');
let cheerio = require('cheerio');
let moment = require('moment');

let getPageCount = (leagueId) => {
    return new Promise((resolve, reject) => {
        request.post('https://sports.bwin.com/en/sports/indexmultileague')
            .send({
                sportId: 4, //football
                leagueIds: leagueId || 46, //  默认值，方便测试 de bundersliga
                page: 0,
                categoryIds: 25 // way3
            })
            .set('X-Requested-With', 'XMLHttpRequest')
            .set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
            .end(function (err, res) {
                if (err) {
                    reject(err);
                }
                let $ = cheerio.load(res.text);
                let wrapper = $('#markets');
                let totalGames = parseInt(wrapper.attr('data-count'));
                let pageCount = $('.markets-pager__pages-count').text().trim();//多少页结果
                if (pageCount) {
                    pageCount = pageCount.split('|');
                    pageCount = pageCount[1].trim();
                    pageCount = pageCount.split('');
                    pageCount = parseInt(pageCount[0].trim());
                } else {
                    pageCount = 1;
                }
                console.info('find ' + totalGames + ' games in ' + pageCount + ' pages');
                resolve({totalGames,pageCount});
            });
    });
};

let fetchRequest = (leagueId, pageIndex, preGames,onSuccess) => {
    let parsedGames = preGames || [];
    request.post('https://sports.bwin.com/en/sports/indexmultileague')
        .send({
            sportId: 4, //football
            leagueIds: leagueId || 46, //  默认值，方便测试 de bundersliga
            page: pageIndex,
            categoryIds: 25 // way3
        })
        .set('X-Requested-With', 'XMLHttpRequest')
        .set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
        .end(function (err, res) {
            if (err) {
                throw err;
            } else {
                let now = new Date();
                let $ = cheerio.load(res.text);
                let wrapper = $('#markets');
                let totalGames = parseInt(wrapper.attr('data-count'));
                let leagueIdWrapper = $('.marketboard-event-group__header-league-favicon', wrapper);
                let events = $('.marketboard-event-group__item-container--level-1', wrapper);//联赛
                let eventLength = events.length;
                if (eventLength !== leagueIdWrapper.length) {
                    throw new Error('Multi leagues miss match!');
                }
                for (let m = 0; m < eventLength; m++) {
                    let currentLeagueId = parseInt($('i', leagueIdWrapper[m]).attr('data-league').substr(2));
                    console.info('find games for league ' + currentLeagueId);
                    let groupWrapper = $('.marketboard-event-group__item-container--level-2', events[m]);
                    let groups = $('.marketboard-event-group__item--sub-group', groupWrapper);// 天
                    let l = groups.length;
                    for (let i = 0; i < l; i++) {
                        let group = groups[i];
                        let dateTemp = $('.marketboard-event-group__header-content--level-3', group).text().trim();
                        dateTemp = dateTemp.split('-');
                        dateTemp = dateTemp[1].trim();
                        let gameWrapper = $('.marketboard-event-group__item-container--level-3', group);
                        let games = $('.marketboard-event-group__item--event', gameWrapper);
                        // console.info(dateTemp + ' has ' + games.length + ' games');
                        let gl = games.length;
                        for (let j = 0; j < gl; j++) {
                            let gameDom = games[j];
                            let timeTemp = $('.marketboard-event-without-header__market-time', gameDom).text().trim();
                            let teams = $('.mb-option-button__option-name', gameDom);
                            let rates = $('.mb-option-button__option-odds', gameDom);
                            let matchId = parseInt($('.mb-option-button.mb-option-button--3-way',gameDom).attr('data-option').split('/')[2]);
                            let tl = teams.length;
                            let rl = rates.length;
                            if (tl === 3 && rl === 3) {
                                let t1 = $(teams[0]).text().trim();// 左队
                                let t2 = $(teams[2]).text().trim();// 右队
                                let t0 = $(teams[1]).text().trim();// 平
                                let r1 = $(rates[0]).text().trim();
                                let r2 = $(rates[2]).text().trim();
                                let r0 = $(rates[1]).text().trim();
                                if (r1.indexOf('/') > 0) {
                                    throw new Error('wrong rate format, e.g. r1 = ' + r1 + ' and r2 = ' + r2 + ' and r0 = ' + r0);
                                }
                                r1 = Number(r1);
                                r2 = Number(r2);
                                r0 = Number(r0);
                                // console.info('\tGame on ' + timeTemp + ' is ' + t1 + '(' + r1 + ')' + ' | ' + t0 + '(' + r0 + ')' + ' | ' + t2 + '(' + r2 + ')');
                                let parsedGame = {
                                    leagueId: currentLeagueId,
                                    matchId,
                                    lastUpdate:now,
                                    time: moment(dateTemp + ' ' + timeTemp, 'M/D/YYYY h:m A').format('YYYY-MM-DD HH:mm'),
                                    t1, t2, t0, r1, r2, r0
                                };
                                parsedGames.push(parsedGame);
                                onSuccess();
                            } else {
                                throw new Error('3way error');
                            }
                        }
                    }
                }
            }
        });
};

exports.fetch = (leagueId) => {
    return new Promise((resolve, reject) => {
        let parsedGames = [];
        let fetchPromises = [];
        getPageCount(leagueId).then((result) => {
            for (let pageIndex = 0; pageIndex < result.pageCount; pageIndex++) {
                fetchPromises.push(new Promise((resolve, reject) => {
                    try {
                        fetchRequest(leagueId, pageIndex, parsedGames,resolve);
                    } catch (err) {
                        reject(err);
                    }
                }));
            }
            Promise.all(fetchPromises).then(() => {
                console.info('totally find ' + parsedGames.length + ' parsed games');
                if(parsedGames.length !== result.totalGames){
                    reject(new Error('Can\'t find enough games'));
                }
                resolve(parsedGames);
            }, (err) => {
                reject(err);
            });
        }, (err) => {
            reject(err);
        });
    });
};

// request.get('http://ls.betradar.com/ls/feeds/?/bwinlivescore/en/Europe:Berlin/gismo/event_ticker_init/100')
//     .end(function (err, res) {
//         if(err){
//             console.error(err);
//         }
//         let docs = res.body.doc;
//         let docL = docs.length;
//         let eventCount = 0;
//         for(let i = 0;i< docL;i++){
//             let data = docs[i].data;
//             let dataL = data.length;
//             for(let j = 0;j < dataL;j++){
//                 let event = data[j];
//                 eventCount++;
//                 if(event.matchid === 9929255){
//                     console.info(event);
//                 }
//                 console.info(event.matchid);
//             }
//         }
//         console.info('find ' + eventCount + ' events');
//         // console.info(res.body.doc[0].data[0]);
//     });