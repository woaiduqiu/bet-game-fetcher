'use strict';
const db = require('./db.js');//初始化数据库连接，这个变量不需要使用

//所有数据库操作都通过model
const mTempGame = require('./tempGameModel.js');


const rateSource = require('./fetch.js');

let leagueIds = [
    43,//Bundesliga
    16108,//Spain La Liga
    46,//England Premier league
    42,//Italy Serie A
    4131,//France Ligue 1
    13914,//Championsleague Group A
    13915,//Championsleague Group B
    13916,//Championsleague Group C
    13917,//Championsleague Group D
    13918,//Championsleague Group E
    13922,//Championsleague Group F
    13920,//Championsleague Group G
    13921,//Championsleague Group H
    18254,//Europeanleague Group A
    18256,//Europeanleague Group B
    18258,//Europeanleague Group C
    18260,//Europeanleague Group D
    18262,//Europeanleague Group E
    18264,//Europeanleague Group F
    18266,//Europeanleague Group G
    18268,//Europeanleague Group H
    18723,//Europeanleague Group I
    18724,//Europeanleague Group J
    18725,//Europeanleague Group K
    18726,//Europeanleague Group L
    14443,//WC2018 Qualification Europe Group A
    14444,//WC2018 Qualification Europe Group B
    14445,//WC2018 Qualification Europe Group C
    14446,//WC2018 Qualification Europe Group D
    14447,//WC2018 Qualification Europe Group E
    14448,//WC2018 Qualification Europe Group F
    14449,//WC2018 Qualification Europe Group G
    14450,//WC2018 Qualification Europe Group H
    14451//WC2018 Qualification Europe Group I
    ];


function saveHandler(err,obj){
    if(err){
        console.warn(err);
    }
}

rateSource.fetch(leagueIds.join(','))
    .then((games) => {
        console.info(games[0]);
        let gamePromises = [];
        for(let game of games){
           gamePromises.push(mTempGame.findOneAndUpdate({matchId:game.matchId},game,{upsert:true,new:true,setDefaultsOnInsert:true}).exec());
        }
        return Promise.all(gamePromises);
    },(fetchErr) => {
        // error handling herer
        console.error(fetchErr);
        process.exit(0);
    }).then(() => {
        console.info('successfully processed all games');
        process.exit(0);
    },(err) => {
        console.error(err);
        console.error('the first error query');
    });