'use strict';
let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let gameSchema = new Schema({
    leagueId:Number,
    matchId:{type:Number,index:{unique:true}},
    time:String,
    t1:String,
    t2:String,
    t0:String,
    r1:Number,
    r2:Number,
    r0:Number,
    done:{type:Boolean,default:false},
    p1:{type:Number,default:0},
    p2:{type:Number,default:0},
    lastUpdate:Date
});

gameSchema.index({
    time:-1,
    t1:1,
    t2:1
});

let TempGame = module.exports = mongoose.model('TempGame',gameSchema);